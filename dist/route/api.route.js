"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const faculty_controller_1 = require("../controller/faculty-controller");
const student_controller_1 = require("../controller/student-controller");
const express_1 = require("express");
const main_impl_route_1 = require("./main-impl.route");
const group_controller_1 = require("../controller/group-controller");
const section_controller_1 = require("../controller/section-controller");
const apiRoute = (0, express_1.Router)();
const studentRoute = new main_impl_route_1.MainImplRoute(new student_controller_1.StudentController());
const groupRoute = new main_impl_route_1.MainImplRoute(new group_controller_1.GroupController());
const facultyRoute = new main_impl_route_1.MainImplRoute(new faculty_controller_1.FacultyController());
const sectionRoute = new main_impl_route_1.MainImplRoute(new section_controller_1.SectionController());
apiRoute.use("/student", studentRoute.route);
apiRoute.use("/group", groupRoute.route);
apiRoute.use("/faculty", facultyRoute.route);
apiRoute.use("/section", sectionRoute.route);
exports.default = apiRoute;
//# sourceMappingURL=api.route.js.map